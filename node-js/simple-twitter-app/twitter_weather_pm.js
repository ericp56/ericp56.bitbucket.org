//node libraries
var http = require('http');  //used for Web service calls
var Stream = require('user-stream'); //used to recieve twitter feed
var Twitter = require('twitter-node-client').Twitter; //used to send messages

//Set up the twitter API
var config = {
    "consumerKey": process.env.CKEY,
    "consumerSecret": process.env.CSEC,
    "accessToken": process.env.ATOKEN,
    "accessTokenSecret": process.env.ATOKENSEC,
    "callBackUrl": process.env.CALLBACKURL
}
var twitter = new Twitter(config);
//Set up twitter stream
var stream = new Stream({
    "consumer_key": config.consumerKey,
    "consumer_secret": config.consumerSecret,
    "access_token_key": config.accessToken,
    "access_token_secret": config.accessTokenSecret 
});

//Common Callback functions
var error = function (err, response, body) {
    console.log('ERROR [%s]', JSON.stringify(err));
};
var success = function (data) {
    console.log('Data [%s]', data);
};

//Business Rule Callbacks
function handleIncomingPM(json) {
    console.log(json);
    if(json && json.direct_message && json.direct_message.text && json.direct_message.sender_id)
        getAndReportWeather(json.direct_message.text, json.direct_message.sender_id);
}
function getAndReportStatus(zipCode, twitterId) {
    var get_options = {
        host: 'api.openweathermap.org',
        port: '80',
        path: '/data/2.5/weather?zip=ZIPCODE,us'.replace(/ZIPCODE/, zipCode);
        method: 'GET',
            headers: {
            }
    };
    var get_req = http.request(get_options, function(res) {processGetResponse(res, twitterId)});
    get_req.end();
}
function processGetResponse(res, twitterId) {
    var bodyChunks = [];
    res.setEncoding('utf8');
    res.on('data', function (chunk) {
        bodyChunks.push(chunk);
    }).on('end', function() {
        processStatusResults(eval(Buffer.concat(bodyChunks)), twitterId);
    });
}
function processStatusResults(result, twitterId) {
    var status = "No status available";
    try {
        status = result.weather[0].description;
    } catch(e) {
    }
    twitter.postCustomApiCall('/direct_messages/new.json',{user_id: twitterId, status}, error, success);
}

//OK, create the stream listener and get to work
stream.stream();
stream.on('data', handleIncomingPM);
